# 圆形取色盘

#### 介绍
https://www.cnblogs.com/xiao-tie/p/16587737.html


#### 安装教程

gradle 7.0以上的在 settings.gradle 添加：

```
dependencyResolutionManagement {
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    repositories {
        google()
        mavenCentral()
        maven {url "https://gitee.com/yangguizhong/circular-color-plate/raw/master"}
        jcenter() // Warning: this repository is going to shut down soon
    }
}
```
gradle 7.0以下的在根 build.gradle 添加：

```
allprojects {
    repositories {
        google()
        mavenCentral()
        maven {url "https://gitee.com/yangguizhong/circular-color-plate/raw/master"}
    }
}
```
build.gradle 添加：

```
 implementation 'com.xiaotie.colorPicker:colorPicker:1.1.0'
```


#### 使用说明


```
 <com.wifiled.ipixels.view.RoundColorPaletteHSV360
            android:id="@+id/color_picker"
            android:layout_width="300dp"
            android:layout_height="300dp"
            app:gap="1dp"//取色背景和背景边框的间隙
            app:colorRadius="20dp"//取色圆的半径
            app:colorStroke="3dp"//取色圆的边框宽度
            app:stroke="8dp"//背景边框的宽度
            app:strokeColor="#FFF7A6"//背景边框的颜色
            app:isOutOfBounds="true"//是否超出边框
            app:colorStrokeColor="#2E3C3C"//取色圆的边框宽度
            app:layout_constraintBottom_toBottomOf="parent"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toTopOf="parent" />
```
```
colorPickerView.setColorChangeCallBack(object:RoundColorPaletteHSV360.ColorChangeCallBack{
                override fun onChange(color: Int) {
                  //返回的颜色
                }

            })

```



